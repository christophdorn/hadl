package ac.at.tuwien.dsg.hadl.framework.test.runtime.gdrive;

import java.util.Random;

import rx.subjects.BehaviorSubject;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.AbstractAsyncSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.LoadEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.OperativeCollaboratorEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.OperativeObjectEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensingScope;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabLink;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabObject;
import at.ac.tuwien.dsg.hadl.schema.core.TCollaborator;
import at.ac.tuwien.dsg.hadl.schema.core.TObjectRef;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleFileDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleUserDescriptor;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;


public class SimpleTestingSensor extends AbstractAsyncSensor {

	private ModelTypesUtil mtu = null;
	private Random random = new Random();
	private int config = 0;

	public SimpleTestingSensor(ModelTypesUtil mtu, int config) {
		super();
		this.mtu = mtu;
		this.config = config;
	}

	@Override
	public void asyncLoadOnce(BehaviorSubject<LoadEvent> subject,
			SensingScope scope) {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {			
			e.printStackTrace();
		}
		switch(config)
		{
		case 0:
			behavior0(subject, scope);
			break;
		case 1:
			behavior1(subject, scope);
			break;	
		case 2:
			behavior2(subject, scope);
			break;
		case 3:
			behavior3(subject, scope);
			break;	
		default:
			System.err.println("SensorConfig unsupported: "+config);
		}
						
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {			
			e.printStackTrace();
		}
		subject.onCompleted();

	}

	private void behavior0(BehaviorSubject<LoadEvent> subject, SensingScope sensingScope)
	{
		int newId = random.nextInt(100)+10;
		if (super.oc != null)
		{			
			OperativeCollaboratorEvent oce2 = new OperativeCollaboratorEvent((TCollaborator)mtu.getById("GoogleDriveUser"), super.oc, sensingScope); // need to extract from TActivityScope
			TGoogleUserDescriptor gud1 = new TGoogleUserDescriptor();
			gud1.setEmail("someone"+newId+"@google.com");
			gud1.setId("googleId"+newId);
			oce2.getDescriptors().add(gud1);
			TCollabObject co = (TCollabObject) mtu.getById("GoogleFile");
			oce2.addLoadedViaOppositeActionType(co.getAction().get(2)); 		
			subject.onNext(oce2);
			
			OperativeObjectEvent oce = new OperativeObjectEvent((TCollabObject)mtu.getById("GoogleFile"), super.oc, sensingScope);
			TGoogleFileDescriptor gfd2 = new TGoogleFileDescriptor();
			gfd2.setName("Mock file "+newId);
			gfd2.setId("dummyId"+newId);	
			oce.getDescriptors().add(gfd2);			
			oce.addLoadedViaRef((TObjectRef)mtu.getById("templateFile"));
			subject.onNext(oce);
		}
		else
		{							
			OperativeObjectEvent oce = null;
			if (super.oc1 != null)
			  oce = new OperativeObjectEvent((TCollabObject)mtu.getById("GoogleFile"), oc1, sensingScope);
			else
			  oce = new OperativeObjectEvent((TCollabObject)mtu.getById("GoogleFile"), oc2, sensingScope);	
			TGoogleFileDescriptor gfd2 = new TGoogleFileDescriptor();
			gfd2.setName("Mock file "+newId);
			gfd2.setId("dummyId"+newId);
			oce.getDescriptors().add(gfd2);
			oce.addLoadedViaLinkType((TCollabLink)mtu.getById("google-drive-owning-file"));
			subject.onNext(oce);
		}
	}
	
	private void behavior1(BehaviorSubject<LoadEvent> subject, SensingScope sensingScope)
	{
		String newId = "2";
		if (super.oc != null)
		{			
			OperativeCollaboratorEvent oce2 = new OperativeCollaboratorEvent((TCollaborator)mtu.getById("GoogleDriveUser"), super.oc, sensingScope); // need to extract from TActivityScope
			
			oce2.getDescriptors().add(GoogleModelLoader.createUserDescriptor("user"+newId));
			TCollabObject co = (TCollabObject) mtu.getById("GoogleFile");
			oce2.addLoadedViaOppositeActionType(co.getAction().get(2)); 		
			subject.onNext(oce2);
			
			OperativeObjectEvent oce = new OperativeObjectEvent((TCollabObject)mtu.getById("GoogleFile"), super.oc, sensingScope);			
			oce.getDescriptors().add(GoogleModelLoader.createFileDescriptor("file"+newId));			
			oce.addLoadedViaRef((TObjectRef)mtu.getById("templateFile"));
			subject.onNext(oce);
		}
		else
		{							
			OperativeObjectEvent oce = null;
			if (super.oc1 != null)
			  oce = new OperativeObjectEvent((TCollabObject)mtu.getById("GoogleFile"), oc1, sensingScope);
			else
			  oce = new OperativeObjectEvent((TCollabObject)mtu.getById("GoogleFile"), oc2, sensingScope);				
			oce.getDescriptors().add(GoogleModelLoader.createFileDescriptor("file"+newId));
			oce.addLoadedViaLinkType((TCollabLink)mtu.getById("google-drive-owning-file"));
			subject.onNext(oce);
		}
	}
	
	private void behavior2(BehaviorSubject<LoadEvent> subject, SensingScope sensingScope)
	{
		String newId = "2";
		if (super.oc != null)
		{			
			OperativeObjectEvent oce = new OperativeObjectEvent((TCollabObject)mtu.getById("GoogleFile"), super.oc, sensingScope);			
			oce.getDescriptors().add(GoogleModelLoader.createFileDescriptor("file"+newId));			
			oce.addLoadedViaRef((TObjectRef)mtu.getById("templateFile"));
			oce.addLoadedViaRef((TObjectRef)mtu.getById("fileInfolder")); // doesnt make sense semantically, but ok for testing			
			subject.onNext(oce);
		}
	}
	
	private void behavior3(BehaviorSubject<LoadEvent> subject, SensingScope sensingScope)
	{
		String newId = "2";
		if (super.oc != null)
		{			
			OperativeObjectEvent oce = new OperativeObjectEvent((TCollabObject)mtu.getById("GoogleFile"), super.oc, sensingScope);			
			oce.getDescriptors().add(GoogleModelLoader.createFileDescriptor("file"+newId));			
			oce.addLoadedViaRef((TObjectRef)mtu.getById("templateFile"));			
			subject.onNext(oce);
		}
	}
	
	@Override
	public void asyncAcquire(TActivityScope forScope,
			TOperationalComponent sensingOrigin,
			BehaviorSubject<SensorEvent> subject) {
		subject.onCompleted();
	}

	@Override
	public void asyncAcquire(TActivityScope forScope,
			TOperationalConnector sensingOrigin,
			BehaviorSubject<SensorEvent> subject) {
		subject.onCompleted();
	}

	@Override
	public void asyncAcquire(TActivityScope forScope,
			TOperationalObject sensingOrigin,
			BehaviorSubject<SensorEvent> subject) {
		subject.onCompleted();		
	}

}
