package at.ac.tuwien.dsg.hadl.framework.runtime.events;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;

import at.ac.tuwien.dsg.hadl.schema.core.TCollabRef;
import at.ac.tuwien.dsg.hadl.schema.core.TCollaborator;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;

public class OperativeCollaboratorEvent extends LoadEvent 
{
	private Triple<TOperationalComponent, TOperationalConnector,TOperationalObject> originCompConnObj = null;
	private TCollaborator collaboratorType = null;	
	private Set<TCollabRef> loadedViaRef = new HashSet<TCollabRef>();		
	
	public OperativeCollaboratorEvent(TCollaborator collaboratorType, TOperationalObject origin, SensingScope sensingScope) {
		super(sensingScope);
		this.collaboratorType = collaboratorType;
		this.originCompConnObj = new ImmutableTriple<TOperationalComponent, TOperationalConnector,TOperationalObject>(null, null, origin);
	}
	
	public OperativeCollaboratorEvent(TCollaborator collaboratorType, TOperationalConnector origin, SensingScope sensingScope) {
		super(sensingScope);
		this.collaboratorType = collaboratorType;
		this.originCompConnObj = new ImmutableTriple<TOperationalComponent, TOperationalConnector,TOperationalObject>(null, origin, null);
	}
	
	public OperativeCollaboratorEvent(TCollaborator collaboratorType, TOperationalComponent origin, SensingScope sensingScope) {
		super(sensingScope);
		this.collaboratorType = collaboratorType;
		this.originCompConnObj = new ImmutableTriple<TOperationalComponent, TOperationalConnector,TOperationalObject>(origin, null, null);
	}
	
	public Set<TCollabRef> getLoadedViaRef() {
		return loadedViaRef;
	}
	public void addLoadedViaRef(TCollabRef loadedViaRef) {
		this.loadedViaRef.add(loadedViaRef);
	}
	public TCollaborator getCollaboratorType() {
		return collaboratorType;
	}
	public void setCollaboratorType(TCollaborator collaboratorType) {
		this.collaboratorType = collaboratorType;
	}
	public Triple<TOperationalComponent, TOperationalConnector,TOperationalObject> getCompConnObjOrigin() {
		return originCompConnObj;
	}

	@Override
	public String getOriginId() {
		return (getCompConnObjOrigin().getLeft() == null ? "": getCompConnObjOrigin().getLeft().getId()) + 
				(getCompConnObjOrigin().getMiddle() == null ? "": getCompConnObjOrigin().getMiddle().getId()) +
						(getCompConnObjOrigin().getRight() == null ? "": getCompConnObjOrigin().getRight().getId());
		}
	
	
}
