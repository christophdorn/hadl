package at.ac.tuwien.dsg.hadl.framework.runtime.intf;

import java.util.ArrayList;
import java.util.List;

public class InsufficientModelInformationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8327964666257789345L;

	private List<String> missingInformation = new ArrayList<String>();
	
	public InsufficientModelInformationException(String msg, String missingInformationEntry) {
		super(msg);
		addMissingInformationEntry(missingInformationEntry);
	}

	public List<String> getMissingInformation() {
		return missingInformation;
	}
	
	public void addMissingInformationEntry(String entry)
	{
		missingInformation.add(entry);
	}

	
	
	
}
