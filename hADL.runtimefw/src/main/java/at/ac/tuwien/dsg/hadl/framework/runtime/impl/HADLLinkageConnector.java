package at.ac.tuwien.dsg.hadl.framework.runtime.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.MessageFormatMessage;

import at.ac.tuwien.dsg.hadl.framework.runtime.intf.*;
import at.ac.tuwien.dsg.hadl.schema.core.*;
import at.ac.tuwien.dsg.hadl.schema.runtime.*;
import rx.Observable;
import rx.Observer;
import rx.subjects.BehaviorSubject;


public class HADLLinkageConnector 
{
	protected static final Logger logger = LogManager.getLogger(HADLLinkageConnector.class);
	
	private TActivityScope scope;
	private StatusAnnouncer sa;
	@Inject private SurrogateFactoryResolver sfr; 
	@Inject private Executable2OperationalTransformer e2o; 
	@Inject private RuntimeRegistry reg; 
	@Inject private ModelTypesUtil mqu; 
	@Inject private HADLruntimeModel hrm; 
	
	public HADLLinkageConnector()
	{
		sa = new StatusAnnouncer();	
	}
	
	public Observable<StatusEvent> init(TActivityScope scope)
	{
		if (this.scope == null)
		{
			this.scope = scope;
			logger.info(new MessageFormatMessage("Initializing with scope: {0}", scope.getId()));
			sa.updateStatus(new StatusEvent(this.scope.getId(), EhADLstatus.INIT_COMPLETED));
		}
		else{
			logger.warn(new MessageFormatMessage("Already initialized, ignoring potentially new scope: ", scope.getId()));
			sa.updateErrorStatus(new Throwable("Already initialized"));			
		}
			
		return sa.getSubject();
	}
		
	
	public Observable<SurrogateEvent> acquireResourcesAsElements(List<Entry<THADLarchElement, TResourceDescriptor>> mapping)
	{
		sa.updateStatus(new StatusEvent(this.scope.getId(), EhADLstatus.ACQUIRING_INPROGRESS));
		List<Observable<SurrogateEvent>> observables = new ArrayList<Observable<SurrogateEvent>>();
		try {
		for (Entry<THADLarchElement, TResourceDescriptor> entry : mapping)
		{	
			
		// TODO LATER check if THADLarchElement spec is in scope -- assume so for now			
						
			TResourceDescriptor rd = entry.getValue();
			SurrogateFactory sf = sfr.resolveFactoryFrom(entry.getKey()).get(0); //just take first
			
		// if so create RuntimeElement (and copy elements from spec, + create ref via registry)
		//	for OperationalComponents/Connectors/Objects/ and Refs
		//  and create & initiate surrogate			
			if (entry.getKey() instanceof THumanComponent)
			{
				THumanComponent el = (THumanComponent)entry.getKey();
				TOperationalComponent oc = e2o.toOperationalCopy(el);
				oc.getResourceDescriptor().add(rd);
				oc.setState(TOperationalState.PRESCRIBED_EXISTING);				
				ICollaboratorSurrogate surr = sf.getInstance(el);				
				TSurrogateInstanceRef ref = reg.registerSurrogate(surr, rd);
				surr.setSurrogateInstanceRef(ref);
				oc.setOperationalViaSurrogate(ref);
				oc.setInstanceOf(mqu.lookupTypeRef(el));
				hrm.addComponent(oc);
				observables.add(surr.acquire(scope, oc));
			}
			else if (entry.getKey() instanceof TCollabConnector)
			{
				TCollabConnector el = (TCollabConnector)entry.getKey();
				TOperationalConnector oc = e2o.toOperationalCopy(el);
				oc.getResourceDescriptor().add(rd);
				oc.setState(TOperationalState.PRESCRIBED_EXISTING);				
				ICollaboratorSurrogate surr = sf.getInstance(el);								
				TSurrogateInstanceRef ref = reg.registerSurrogate(surr, rd);
				surr.setSurrogateInstanceRef(ref);
				oc.setOperationalViaSurrogate(ref);
				oc.setInstanceOf(mqu.lookupTypeRef(el));
				hrm.addConnector(oc);
				observables.add(surr.acquire(scope, oc));
			}			
			else if (entry.getKey() instanceof TCollabObject)
			{
				TCollabObject el = (TCollabObject)entry.getKey();
				TOperationalObject oo = e2o.toOperationalCopy(el);
				oo.getResourceDescriptor().add(rd);
				oo.setState(TOperationalState.PRESCRIBED_EXISTING);				
				IObjectSurrogate surr = sf.getInstance(el);
				TSurrogateInstanceRef ref = reg.registerSurrogate(surr, rd);
				surr.setSurrogateInstanceRef(ref);
				oo.setOperationalViaSurrogate(ref);
				oo.setInstanceOf(mqu.lookupTypeRef(el));
				hrm.addObject(oo);
				observables.add(surr.acquire(scope, oo));
			}		

		}
		} catch (MultiFactoryException | InsufficientModelInformationException e) {							
			Observable<SurrogateEvent> oe = Observable.error(e);
			observables.add(oe);								
		}
		Observable<SurrogateEvent> ob = Observable.merge(observables);
		ob.subscribe(new GenericObserver(EhADLstatus.ACQUIRING_COMPLETED, EhADLstatus.ACQUIRING_INCOMPLETED));		
		return ob;		
	}
	
	public Observable<SurrogateEvent> buildRelations(List<Entry<TCollaborator, TCollaborator>> fromTo, TCollabRef relationType, boolean doRemove)
	{
		// find both surrogates, then let both now to build, or remove relationship type --> implies, same relationshiptype between same pair can exist only once				
		List<Observable<SurrogateEvent>> observables = new ArrayList<Observable<SurrogateEvent>>();			
		for (Entry<TCollaborator, TCollaborator> entry : fromTo)
		{
			TOperationalComponent compFrom = entry.getKey() instanceof TOperationalComponent ? (TOperationalComponent)entry.getKey() : null;
			TOperationalComponent compTo = entry.getValue() instanceof TOperationalComponent ? (TOperationalComponent)entry.getValue() : null;
			TOperationalConnector connFrom = entry.getKey() instanceof TOperationalConnector ? (TOperationalConnector)entry.getKey() : null;
			TOperationalConnector connTo = entry.getValue() instanceof TOperationalConnector ? (TOperationalConnector)entry.getValue() : null;
			
			if ((compFrom == null && connFrom == null) || (compTo == null && connTo == null))
			{
				return(Observable.error(new Exception("No OperationalComp/Conn found to establish/remove Relation between: "+entry.getKey().getId()+" and "+entry.getValue().getId())));
			}
			TSurrogateInstanceRef fromRef = compFrom == null ? connFrom.getOperationalViaSurrogate() : compFrom.getOperationalViaSurrogate();
			TSurrogateInstanceRef toRef = compTo == null ? connTo.getOperationalViaSurrogate() : compTo.getOperationalViaSurrogate();
						
			ICollaboratorSurrogate fromSurr = reg.getCollaboratorSurrogate(fromRef);
			ICollaboratorSurrogate toSurr = reg.getCollaboratorSurrogate(toRef);
			TOperationalCollabRef ref = null;
			if (fromSurr == null && toSurr == null)
			{
				return(Observable.error(new Exception("No Surrogates found to establish/remove Relation between: "+entry.getKey().getId()+" and "+entry.getValue().getId())));
			}
			else
			{// add relation in runtime model
				if (doRemove)
				{
					ref = hrm.removeCollabRef(new ImmutableTriple<THADLarchElement, THADLarchElement, TCollabRef>(entry.getKey(), entry.getValue(), relationType));
				}
				else
				{
					ref = hrm.addCollabRef(entry.getKey(), entry.getValue(), relationType, true);
				}
			}			
			if (fromSurr != null)
			{							
				observables.add(connTo == null ? fromSurr.relating(compTo, relationType, true, doRemove, ref) : fromSurr.relating(connTo, relationType, true, doRemove, ref));				
			}						
			if (toSurr != null)
			{				
				observables.add(connFrom == null ? fromSurr.relating(compFrom, relationType, false, doRemove, ref) : fromSurr.relating(connFrom, relationType, false, doRemove, ref));				
			}
			
		}
		return Observable.merge(observables);
	}
	
	public Observable<SurrogateEvent> buildRelations(List<Entry<TOperationalObject, TOperationalObject>> fromTo, TObjectRef relationType, boolean doRemove)
	{
		// find both surrogates, then let both now to build, or remove relationship type --> implies, same relationshiptype between same pair can exist only once		
		List<Observable<SurrogateEvent>> observables = new ArrayList<Observable<SurrogateEvent>>();			
		for (Entry<TOperationalObject, TOperationalObject> entry : fromTo)
		{
			IObjectSurrogate fromSurr = reg.getObjectSurrogate(entry.getKey().getOperationalViaSurrogate());
			IObjectSurrogate toSurr = reg.getObjectSurrogate(entry.getValue().getOperationalViaSurrogate());
			TOperationalObjectRef ref = null; 
			if (fromSurr == null && toSurr == null)
			{
				return(Observable.error(new Exception("No Surrogates found to establish/remove Relation between: "+entry.getKey().getId()+" and "+entry.getValue().getId())));
			}
			else
			{// add relation in runtime model
				if (doRemove)
				{
					ref = hrm.removeObjectRef(new ImmutableTriple<THADLarchElement, THADLarchElement, TObjectRef>(entry.getKey(), entry.getValue(), relationType));
				}
				else
				{
					ref = hrm.addObjectRef(entry.getKey(), entry.getValue(), relationType, true);
				}
			}			
			if (fromSurr != null)
			{								
				observables.add(fromSurr.relating(entry.getValue(), relationType, true, doRemove, ref));				
			}			
			if (toSurr != null)
			{								
				observables.add(fromSurr.relating(entry.getKey(), relationType, false, doRemove, ref));									
			}
			
		}
		return Observable.merge(observables);		
	}
	
	
	// wireActionsBetweenInstances
	
	// takes inner product of collaborator types and wireTypes
	public Observable<SurrogateEvent> wireActionsOfCollaboratorTypes(List<TCollaborator> collabTypes, List<TCollabLink> wireTypes)
	{
		sa.updateStatus(new StatusEvent(this.scope.getId(), EhADLstatus.WIRING_INPROGRESS));
		List<Observable<SurrogateEvent>> observables = new ArrayList<Observable<SurrogateEvent>>();

		// for each given collaborator type: retrieve all instances 
		// 		and check for each, which of the wiretypes apply
		//			then extract opposite collab object
		for(TCollaborator cType : collabTypes)
		{
			List<TCollabLink> matchingWireTypes = mqu.getLinkableWires(wireTypes, cType);						
			for (TCollabLink cl : matchingWireTypes)
			{
				TAction a2 = cl.getObjActionEndpoint();
				TCollabObject oType = mqu.getObjectForAction(a2);
				List<TOperationalObject> objs = hrm.getObjInstancesOfType(mqu.lookupTypeRef(oType));

				TTypeRef cRef = mqu.lookupTypeRef(cType);						
				if (cType instanceof THumanComponent)
				{
					for (TOperationalComponent comp : hrm.getCompInstancesOfType(cRef))
					{
						for (TOperationalObject opObj : objs)
						{
							observables.add(wire(comp, opObj, cl));
						}
					}
				}
				else
				{
					for (TOperationalConnector conn : hrm.getConnInstancesOfType(cRef))
					{	
						for (TOperationalObject opObj : objs)
						{
							observables.add(wire(conn, opObj, cl));
						}
					}
				}
			}

		}

		Observable<SurrogateEvent> ob = Observable.merge(observables);
		ob.subscribe(new GenericObserver(EhADLstatus.WIRING_COMPLETED, EhADLstatus.WIRING_INCOMPLETED));		
		return ob;
	}

	// takes inner product of object types and wireTypes
	public Observable<SurrogateEvent> wireActionsOfObjectTypes(List<TCollabObject> objTypes, List<TCollabLink> wireTypes)
	{
		sa.updateStatus(new StatusEvent(this.scope.getId(), EhADLstatus.WIRING_INPROGRESS));
		List<Observable<SurrogateEvent>> observables = new ArrayList<Observable<SurrogateEvent>>();
		
		// for each given obj type: retrieve all instances 
		// 		and check for each, which of the wiretypes apply
		//			then extract opposite collaborator
		for(TCollabObject objT : objTypes)
		{
			List<TCollabLink> matchingWireTypes = mqu.getLinkableWires(wireTypes, objT);
			List<TOperationalObject> objs = hrm.getObjInstancesOfType(mqu.lookupTypeRef(objT));
			
			for (TOperationalObject opObj : objs)
			{
				for (TCollabLink cl : matchingWireTypes)
				{
					TAction a1 = cl.getCollabActionEndpoint();
					TCollaborator cType = mqu.getCollaboratorForAction(a1);
					// for that type, retrieve all instances in this scope
					TTypeRef cRef = mqu.lookupTypeRef(cType);						
					if (cType instanceof THumanComponent)
					{
						for (TOperationalComponent comp : hrm.getCompInstancesOfType(cRef))
						{
							observables.add(wire(comp, opObj, cl));						
						}
					}
					else
					{
						for (TOperationalConnector conn : hrm.getConnInstancesOfType(cRef))
						{							
							observables.add(wire(conn, opObj, cl));							
						}
					}
				}
			}
		}
						
		Observable<SurrogateEvent> ob = Observable.merge(observables);
		ob.subscribe(new GenericObserver(EhADLstatus.WIRING_COMPLETED, EhADLstatus.WIRING_INCOMPLETED));		
		return ob;
	}
	
	public Observable<SurrogateEvent> wireAllActions(List<TCollabLink> wireTypes)
	{
		sa.updateStatus(new StatusEvent(this.scope.getId(), EhADLstatus.WIRING_INPROGRESS));
		List<Observable<SurrogateEvent>> observables = new ArrayList<Observable<SurrogateEvent>>();		
		for (TCollabLink cl : wireTypes)
		{								
			TAction a2 = cl.getObjActionEndpoint();
			TCollabObject oType = mqu.getObjectForAction(a2);
			// for that type, retrieve all instances in this scope
			List<TOperationalObject> objs = hrm.getObjInstancesOfType(mqu.lookupTypeRef(oType));
			
			TAction a1 = cl.getCollabActionEndpoint();
			TCollaborator cType = mqu.getCollaboratorForAction(a1);
			// for that type, retrieve all instances in this scope
			TTypeRef cRef = mqu.lookupTypeRef(cType);						
			if (cType instanceof THumanComponent)
			{
				for (TOperationalComponent comp : hrm.getCompInstancesOfType(cRef))
				{
					for (TOperationalObject obj : objs)
					{
						observables.add(wire(comp, obj, cl));
					}
				}
			}
			else
			{
				for (TOperationalConnector conn : hrm.getConnInstancesOfType(cRef))
				{
					for (TOperationalObject obj : objs)
					{
						observables.add(wire(conn, obj, cl));
					}
				}
			}
		}		
		Observable<SurrogateEvent> ob = Observable.merge(observables);
		ob.subscribe(new GenericObserver(EhADLstatus.WIRING_COMPLETED, EhADLstatus.WIRING_INCOMPLETED));		
		return ob;		
	}
	
	public Observable<SurrogateEvent> wireAllActions()
	{			
		List<TCollabLink> wireTypes = scope.getLinkRef();
		return wireAllActions(wireTypes);
	}
	
	private Observable<SurrogateEvent> wire(TOperationalComponent comp, TOperationalObject obj, TCollabLink wireType)
	{
		//check in which state the link might already exist: if in PRESCRIBED/DESCRIBED_EXISTING then don't call again
		/*
		 * OTHERWISE call, but what if in PRESCRIBED_NONEXIST: not yet successfully removed, shall we wait (for a removal result), 
		 * or retry and expect the sensing element to be so intelligent as not to override the
		 * new PRESCRIBED_EXISTING with a potential DESCRIBED_NONEXIST or PRESCIPTION_NONEXIST_DENIED?
		 * 
		 * for now lets assume we have an intelligent sensor that doesn't override a nonmatching prescribed state (ever)
		 * */
		TOperationalCollabLink link = hrm.getOperationalLinkForPairAndType(new ImmutableTriple<TOperationalObject, THADLarchElement, TCollabLink>(obj, comp, wireType));
		if (link != null)	
		{
			if (link.getState().equals(TOperationalState.PRESCRIBED_EXISTING) || link.getState().equals(TOperationalState.DESCRIBED_EXISTING))
			{
				return Observable.empty();
			}
			else 
			{
				hrm.renewLink(link);
			}
		}
		else
		{
			link = hrm.addLink(comp, obj, wireType);
		}
		
		List<Observable<SurrogateEvent>> observables = new ArrayList<Observable<SurrogateEvent>>();
		ICollaboratorSurrogate cSurr = reg.getCollaboratorSurrogate(comp.getOperationalViaSurrogate());
		if (cSurr != null)
			observables.add(cSurr.linkTo(link.getCollabActionEndpoint(), obj, link.getObjActionEndpoint(), link));
		IObjectSurrogate oSurr = reg.getObjectSurrogate(obj.getOperationalViaSurrogate());
		if (oSurr != null)
			observables.add(oSurr.linkTo(link.getObjActionEndpoint(), comp, link.getCollabActionEndpoint(), link));
		return Observable.merge(observables);
	}
	
	private Observable<SurrogateEvent> wire(TOperationalConnector conn, TOperationalObject obj, TCollabLink wireType)
	{
		TOperationalCollabLink link = hrm.getOperationalLinkForPairAndType(new ImmutableTriple<TOperationalObject, THADLarchElement, TCollabLink>(obj, conn, wireType));
		if (link != null)	
		{
			if (link.getState().equals(TOperationalState.PRESCRIBED_EXISTING) || link.getState().equals(TOperationalState.DESCRIBED_EXISTING))
			{
				return Observable.empty();
			}
			else 
			{
				hrm.renewLink(link);
			}
		}
		else
		{
			link = hrm.addLink(conn, obj, wireType);
		}
		
		List<Observable<SurrogateEvent>> observables = new ArrayList<Observable<SurrogateEvent>>();	
		ICollaboratorSurrogate cSurr = reg.getCollaboratorSurrogate(conn.getOperationalViaSurrogate());
		if (cSurr != null)
			observables.add(cSurr.linkTo(link.getCollabActionEndpoint(), obj, link.getObjActionEndpoint(), link));
		IObjectSurrogate oSurr = reg.getObjectSurrogate(obj.getOperationalViaSurrogate());
		if (oSurr != null)
			observables.add(oSurr.linkTo(link.getObjActionEndpoint(), conn, link.getCollabActionEndpoint(), link));
		return Observable.merge(observables);
	}
	
	public Observable<SurrogateEvent> startScope()
	{
		sa.updateStatus(new StatusEvent(this.scope.getId(), EhADLstatus.STARTING_INPROGRESS));
		List<Observable<SurrogateEvent>> observables = new ArrayList<Observable<SurrogateEvent>>();
		for (ISurrogate surr : reg.getAllSurrogates())
		{
			observables.add(surr.begin());
		}
		Observable<SurrogateEvent> ob = Observable.merge(observables);
		ob.subscribe(new GenericObserver(EhADLstatus.STARTING_COMPLETED, EhADLstatus.STARTING_INCOMPLETED));		
		return ob;		
	}
	
	
	
	
	// in reverse clean up scope
	public Observable<SurrogateEvent> stopScope()
	{
		sa.updateStatus(new StatusEvent(this.scope.getId(), EhADLstatus.STOPPING_INPROGRESS));
		// Merely a nice way to inform participants they should clean up
		List<Observable<SurrogateEvent>> observables = new ArrayList<Observable<SurrogateEvent>>();
		for (ISurrogate surr : reg.getAllSurrogates())
		{
			observables.add(surr.stop());
		}

		Observable<SurrogateEvent> ob = Observable.merge(observables);
		ob.subscribe(new GenericObserver(EhADLstatus.STOPPING_COMPLETED, EhADLstatus.STOPPING_INCOMPLETED));		
		return ob;		
	}
	
	public Observable<SurrogateEvent> unwireAllActions()
	{
		sa.updateStatus(new StatusEvent(this.scope.getId(), EhADLstatus.WIRING_INPROGRESS));
		// set wires to prescribed non exist
		List<Observable<SurrogateEvent>> observables = new ArrayList<Observable<SurrogateEvent>>();
		for (TOperationalCollabLink ol : hrm.getLink())
		{
			hrm.removeLink(ol); // doesn't actually remove but rather mark as to be removed (PRESCRIBED_NONEXIST)
			// for each endpoint, get respective surrogate, and remove
			Triple<TOperationalObject,TOperationalComponent,TOperationalConnector> triple = hrm.getOperationalPairForLink(ol);
			
			if (triple != null)
			{
				IObjectSurrogate oSurr = reg.getObjectSurrogate(triple.getLeft().getOperationalViaSurrogate());				
				if (triple.getMiddle() == null)
				{ // comp == null
					ICollaboratorSurrogate cSurr = reg.getCollaboratorSurrogate(triple.getRight().getOperationalViaSurrogate());
					observables.add(oSurr.disconnectFrom(ol.getObjActionEndpoint(), triple.getRight(), ol));
					observables.add(cSurr.disconnectFrom(ol.getCollabActionEndpoint(), triple.getLeft(), ol));
				}
				else
				{
					ICollaboratorSurrogate cSurr = reg.getCollaboratorSurrogate(triple.getMiddle().getOperationalViaSurrogate());
					observables.add(oSurr.disconnectFrom(ol.getObjActionEndpoint(), triple.getMiddle(), ol));
					observables.add(cSurr.disconnectFrom(ol.getCollabActionEndpoint(), triple.getLeft(), ol));
				}				
			}
		}		
		Observable<SurrogateEvent> ob = Observable.merge(observables);
		ob.subscribe(new GenericObserver(EhADLstatus.WIRING_COMPLETED, EhADLstatus.WIRING_INCOMPLETED));		
		return ob;		
	}
	
	public Observable<SurrogateEvent> releaseAllResources()
	{
		// set elements to prescribed non exist
		sa.updateStatus(new StatusEvent(this.scope.getId(), EhADLstatus.DISOLVING_INPROGRESS));
		List<Observable<SurrogateEvent>> observables = new ArrayList<Observable<SurrogateEvent>>();		
		observables.add(releaseComponentResources());
		observables.add(releaseConnectorResources());		
		observables.add(releaseObjectResources());		
		Observable<SurrogateEvent> ob = Observable.merge(observables);
		ob.subscribe(new GenericObserver(EhADLstatus.DISOLVING_COMPLETED, EhADLstatus.DISOLVING_INCOMPLETED));		
		return ob;		
	}
	
	public Observable<SurrogateEvent> releaseComponentResources()
	{
		List<Observable<SurrogateEvent>> observables = new ArrayList<Observable<SurrogateEvent>>();
		for (TOperationalComponent oC : hrm.getComponent())
		{
			hrm.removeComponent(oC);
			ICollaboratorSurrogate surrC = reg.getCollaboratorSurrogate(oC.getOperationalViaSurrogate());
			observables.add(surrC.release());
		}
		return Observable.merge(observables);
	}
	
	public Observable<SurrogateEvent> releaseConnectorResources()
	{
		List<Observable<SurrogateEvent>> observables = new ArrayList<Observable<SurrogateEvent>>();
		for (TOperationalConnector oC : hrm.getConnector())
		{
			hrm.removeConnector(oC);
			ICollaboratorSurrogate surrC = reg.getCollaboratorSurrogate(oC.getOperationalViaSurrogate());
			observables.add(surrC.release());
		}
		return Observable.merge(observables);
	}
	
	public Observable<SurrogateEvent> releaseObjectResources()
	{
		List<Observable<SurrogateEvent>> observables = new ArrayList<Observable<SurrogateEvent>>();
		for (TOperationalObject oC : hrm.getObject())
		{
			hrm.removeObject(oC);
			IObjectSurrogate surrC = reg.getObjectSurrogate(oC.getOperationalViaSurrogate());
			observables.add(surrC.release());
		}
		return Observable.merge(observables);
	}
	
	
	
	public void shutdown()
	{		
		sa.shutdown();		
	}
	
	private class StatusAnnouncer
	{
		private BehaviorSubject<StatusEvent> statusSubject = null;
		private ExecutorService dispatcher = Executors.newSingleThreadExecutor();	
		
		public BehaviorSubject<StatusEvent> getSubject()
		{
			if (statusSubject == null)
				statusSubject = BehaviorSubject.create(new StatusEvent("", EhADLstatus.INIT_INPROGRESS));	
			return statusSubject;
		}
		
		public void updateStatus(final StatusEvent event) {
			dispatcher.execute(new Runnable(){
				@Override
				public void run() {
					getSubject().onNext(event);
				}			
			});	
		}	
		
		public void updateErrorStatus(final Throwable e) {
			dispatcher.execute(new Runnable(){
				@Override
				public void run() {
					getSubject().onError(e);
				}			
			});	
		}	
		  		
		
		public void shutdown()
		{		
			logger.debug("Status Accouncer shutting down");			
			dispatcher.execute(new Runnable(){
				@Override
				public void run() {
					getSubject().onCompleted();
				}			
			});
			
			try {
				dispatcher.awaitTermination(1000, TimeUnit.MILLISECONDS);
			} catch (InterruptedException e) {;			
			}					
		}
	}
	
	private class GenericObserver implements Observer<SurrogateEvent>
	{
		 	private EhADLstatus successStatus;
		 	private EhADLstatus failStatus;	
		 	private int errCount = 0;
		 	
			public GenericObserver(EhADLstatus successStatus,
					EhADLstatus failStatus) {
				super();
				this.successStatus = successStatus;
				this.failStatus = failStatus;
			}

			@Override
			public void onCompleted() {				
				sa.updateStatus(new StatusEvent(scope.getId(), (errCount > 0 ? failStatus : successStatus)));										
			}

			@Override
			public void onError(Throwable e) {
				//rethrow error
				sa.updateErrorStatus(e);
			}

			@Override
			public void onNext(SurrogateEvent t) {				
				if (t.getOptionalEx() != null)
					errCount++;
			}			
			
	}
}
