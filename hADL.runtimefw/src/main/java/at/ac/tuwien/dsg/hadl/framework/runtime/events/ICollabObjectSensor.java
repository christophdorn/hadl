package at.ac.tuwien.dsg.hadl.framework.runtime.events;

import rx.Observable;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;

public interface ICollabObjectSensor extends ICollabSensor
{
	public Observable<SensorEvent> acquire(TActivityScope forScope, TOperationalObject sensingOrigin);	
}
