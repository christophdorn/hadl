package at.ac.tuwien.dsg.hadl.framework.runtime.events;

import java.util.HashSet;
import java.util.Set;

import at.ac.tuwien.dsg.hadl.schema.core.TAction;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabLink;
import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;

public abstract class LoadEvent 
{
	private SensingScope sScope = null;	
	private Set<TCollabLink> loadedViaLinkType = new HashSet<TCollabLink>();
	private Set<TAction> loadedViaOppositeActionType = new HashSet<TAction>();
	private Set<TCollabLink> loadedViaSubstructureWireType = new HashSet<TCollabLink>();
	
	LoadEvent(SensingScope sensingScope)
	{
		this.sScope = sensingScope;
	}
	
	
	private HashSet<TResourceDescriptor> descriptors = new HashSet<TResourceDescriptor>();

	public Set<TCollabLink> getLoadedViaLinkType() {
		return loadedViaLinkType;
	}

	public void addLoadedViaLinkType(TCollabLink loadedViaLinkType) {
		this.loadedViaLinkType.add(loadedViaLinkType);
	}

	public Set<TAction> getLoadedViaOppositeActionType() {
		return loadedViaOppositeActionType;
	}

	public void addLoadedViaOppositeActionType(TAction loadedViaOppositeActionType) {
		this.loadedViaOppositeActionType.add(loadedViaOppositeActionType);
	}

	public Set<TCollabLink> getLoadedViaSubstructureWireType() {
		return loadedViaSubstructureWireType;
	}

	public void addLoadedViaSubstructureWireType(
			TCollabLink loadedViaSubstructureWireType) {
		this.loadedViaSubstructureWireType.add(loadedViaSubstructureWireType);
	}

	public HashSet<TResourceDescriptor> getDescriptors() {
		return descriptors;
	}

	public void setDescriptors(HashSet<TResourceDescriptor> descriptors) {
		this.descriptors = descriptors;
	}
	
	
	
	public SensingScope getSensingScope() {
		return sScope;
	}

	public abstract String getOriginId();

}
