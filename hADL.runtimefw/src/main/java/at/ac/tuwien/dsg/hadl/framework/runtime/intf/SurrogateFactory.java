package at.ac.tuwien.dsg.hadl.framework.runtime.intf;

import at.ac.tuwien.dsg.hadl.schema.core.TCollabObject;
import at.ac.tuwien.dsg.hadl.schema.core.TCollaborator;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;


public interface SurrogateFactory 
{

	public ISurrogate getInstance(THADLarchElement type) throws InsufficientModelInformationException;
	
	public ICollaboratorSurrogate getInstance(TCollaborator collaboratorSpec) throws InsufficientModelInformationException;
	
	public IObjectSurrogate getInstance(TCollabObject objectSpec) throws InsufficientModelInformationException;
	
}
