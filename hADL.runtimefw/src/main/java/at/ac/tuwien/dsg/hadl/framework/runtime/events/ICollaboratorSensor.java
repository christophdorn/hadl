package at.ac.tuwien.dsg.hadl.framework.runtime.events;

import rx.Observable;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;

public interface ICollaboratorSensor extends ICollabSensor
{
	public Observable<SensorEvent> acquire(final TActivityScope forScope, TOperationalComponent sensingOrigin);
	
	public Observable<SensorEvent> acquire(final TActivityScope forScope, TOperationalConnector sensingOrigin);
}
