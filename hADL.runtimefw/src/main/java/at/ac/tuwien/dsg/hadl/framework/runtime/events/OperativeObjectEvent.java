package at.ac.tuwien.dsg.hadl.framework.runtime.events;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;

import at.ac.tuwien.dsg.hadl.schema.core.TCollabObject;
import at.ac.tuwien.dsg.hadl.schema.core.TObjectRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;

public class OperativeObjectEvent extends LoadEvent 
{
	private TCollabObject objType = null;	
	private Set<TObjectRef> loadedViaRef = new HashSet<TObjectRef>();	
	private Triple<TOperationalComponent, TOperationalConnector,TOperationalObject> originCompConnObj = null;
	
	public OperativeObjectEvent(TCollabObject sensedObjType, TOperationalObject origin, SensingScope sensingScope) {
		super(sensingScope);
		this.objType = sensedObjType;
		this.originCompConnObj = new ImmutableTriple<TOperationalComponent, TOperationalConnector,TOperationalObject>(null,  null,  origin);
	}
	
	public OperativeObjectEvent(TCollabObject sensedObjType, TOperationalComponent origin, SensingScope sensingScope) {
		super(sensingScope);
		this.objType = sensedObjType;
		this.originCompConnObj = new ImmutableTriple<TOperationalComponent, TOperationalConnector,TOperationalObject>(origin,  null,  null);
	}
	
	public OperativeObjectEvent(TCollabObject sensedObjType, TOperationalConnector origin, SensingScope sensingScope) {
		super(sensingScope);
		this.objType = sensedObjType;
		this.originCompConnObj = new ImmutableTriple<TOperationalComponent, TOperationalConnector,TOperationalObject>(null, origin, null);
	}
	
	public TCollabObject getObjType() {
		return objType;
	}
	public void setObjType(TCollabObject objType) {
		this.objType = objType;
	}
	public Set<TObjectRef> getLoadedViaRef() {
		return loadedViaRef;
	}
	public void addLoadedViaRef(TObjectRef loadedViaRef) {
		this.loadedViaRef.add(loadedViaRef);
	}
	public Triple<TOperationalComponent, TOperationalConnector,TOperationalObject> getCompConnObjOrigin() {
		return originCompConnObj;
	}
	
	@Override
	public String getOriginId() {
		return (getCompConnObjOrigin().getLeft() == null ? "": getCompConnObjOrigin().getLeft().getId()) + 
				(getCompConnObjOrigin().getMiddle() == null ? "": getCompConnObjOrigin().getMiddle().getId()) +
						(getCompConnObjOrigin().getRight() == null ? "": getCompConnObjOrigin().getRight().getId());
		}
}
