package at.ac.tuwien.dsg.hadl.framework.runtime.intf;

import java.util.Hashtable;
import java.util.Map.Entry;

public class MultiException<T extends Exception> 
{
	private Hashtable<String, T> exceptions = new Hashtable<String, T>();
	
	public Hashtable<String, T> getExceptions()
	{
		return exceptions;
	}
	
	public void addException(String correlationId, T exception)
	{
		exceptions.put(correlationId, exception);
	}
	
	public boolean isEmpty()
	{
		return exceptions.isEmpty();
	}
	
	public void mergeIntoThis(MultiException<T> me)
	{		
		for (Entry<String, T> entry : me.getExceptions().entrySet())
		{			 
			this.exceptions.put(entry.getKey(), entry.getValue());
		}
	}
}
