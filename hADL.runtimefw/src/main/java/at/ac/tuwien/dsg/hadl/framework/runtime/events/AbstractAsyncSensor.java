package at.ac.tuwien.dsg.hadl.framework.runtime.events;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import rx.Observable;
import rx.subjects.BehaviorSubject;
import at.ac.tuwien.dsg.hadl.schema.core.TAction;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabRef;
import at.ac.tuwien.dsg.hadl.schema.core.TCollaborator;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;
import at.ac.tuwien.dsg.hadl.schema.runtime.TSurrogateInstanceRef;

public abstract class AbstractAsyncSensor implements ICollaboratorSensor, ICollabObjectSensor
{	
	private ExecutorService dispatcher = Executors.newSingleThreadExecutor();			
	protected TOperationalComponent oc1 = null;
	protected TOperationalConnector oc2 = null;
	protected TOperationalObject oc = null;
	protected TActivityScope scope = null;
	
	
	@Override
	public Observable<SensorEvent> acquire(final TActivityScope forScope,
			final TOperationalComponent sensingOrigin) {
		final BehaviorSubject<SensorEvent> statusSubject = BehaviorSubject.create();
		this.scope = forScope;
		this.oc1 = sensingOrigin;
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncAcquire(forScope, sensingOrigin, statusSubject);
			}			
		});						
		return statusSubject;
	}

	@Override
	public Observable<SensorEvent> acquire(final TActivityScope forScope,
			final TOperationalConnector sensingOrigin) {
		final BehaviorSubject<SensorEvent> statusSubject = BehaviorSubject.create();
		this.scope = forScope;
		this.oc2 = sensingOrigin;
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncAcquire(forScope, sensingOrigin, statusSubject);
			}			
		});						
		return statusSubject;
	}

	public abstract void asyncAcquire(TActivityScope forScope,
			TOperationalComponent sensingOrigin, BehaviorSubject<SensorEvent> subject);

	public abstract void asyncAcquire(TActivityScope forScope,
			TOperationalConnector sensingOrigin, BehaviorSubject<SensorEvent> subject) ;
	

	
	@Override
	public Observable<SensorEvent> acquire(final TActivityScope forScope,
			final TOperationalObject sensingOrigin) {
		final BehaviorSubject<SensorEvent> statusSubject = BehaviorSubject.create();
		this.scope = forScope;
		this.oc = sensingOrigin;
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncAcquire(forScope, sensingOrigin, statusSubject);
			}			
		});						
		return statusSubject;
	}

	public abstract void asyncAcquire(TActivityScope forScope,
			TOperationalObject sensingOrigin, BehaviorSubject<SensorEvent> subject);

	
	
	/* (non-Javadoc)
	 * @see at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollabSensor#loadOnce(at.ac.tuwien.dsg.hadl.framework.runtime.events.SensingScope)
	 */
	@Override
	public Observable<LoadEvent> loadOnce(final SensingScope scope) {
		final BehaviorSubject<LoadEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncLoadOnce(statusSubject, scope);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncLoadOnce(BehaviorSubject<LoadEvent> subject, SensingScope scope);
	// should extract elements it is capable of loading from scope spec (scope spec might include less items than referenced scope instance)
	// load from underlying system the linked/referenced elements and create their resource descriptors
	// return link/ref type, opposite type and resource descriptor(s) for each element
	// distinguish among: collaborators, objects, references, and links, (also structures eventually)


	
}
